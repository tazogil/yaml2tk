# Génére une classe de base tkinter depuis un fichier de configuration YAML

Comme c'est fastidieux de composer une interface (même avec le très simple tkinter), avec ce petit programme il suffit de configurer un fichier YAML puis de le passer dans cette moulinette pour créer la classe de base correspondante.

Ça utilise uniquement grid.

Voici un example de configuration :

![example de fichier de conf](https://framagit.org/tazogil/videotheque/blob/master/app/base/form.yml)

## INSTALLATION

Cloner le projet

```git clone git@framagit.org:tazogil/tkgen.git```

Créer l'environnement virtuel dans le dossier de l'application

```$ virtualenv -p /usr/bin/python3 tkgen_env```

Démarrer l'environnement

```$ source ./tkgen_env/bin/activate```

Installer la librairie requise (pyaml)

$ pip install -r requirements.txt

## UTILISATION

Il faut avoir créer un fichier de configuration yaml, example ci-dessus. 

Puis:

```(tkgen_env)$ python gen.py /chemin/vers/le/fic/de/ma_classe.yaml```

Ce qui va créer /chemin/vers/le/fic/de/ma_classe_base.py

Enfin il faut créer le module ma_classe.py qui étend MaClasseBase :

```
from base import ma_classe_base

class MaClasse(MaClasseBase) :
    
    def __init__(self, master):
        super().__init__(master)

```

Et ajouter les méthodes qu'il faut.
