import yaml
import sys
from os.path import abspath, dirname, join, isfile
from collections import OrderedDict


class Gen:

    default_tpl = """
        self.{name} = {tk}.{_type}({parent}{attributes}) """

    grid_tpl = """
        self.{name}.grid({attributes}) """

    grid_colconf_tpl = """
        self.{name}.grid_columnconfigure({i}, weight={weight}, uniform="{uniform}") """

    grid_rowconf_tpl = """
        self.{name}.grid_rowconfigure({i}, weight={weight}, uniform="{uniform}") """

    button_command_tpl = """
    def {name}_command(self):
        pass
    """

    command_tpl = """
    def {command}(self):
        pass
    """

    bind_tpl = """
        self.{name}.bind("{co}{bind}{cf}", self.{bind_name}) """

    bind_method_tpl = """
    def {bind_name}(self, evt):
       pass """

    treeview_tpl = """
        self.{name} = ttk.Treeview({parent}{attributes})
        self.{name}.heading("#0", text="{text}")
        """

    variable_tpl = """
        self.{name} = tk.{var_type}() """

    image_tpl = """
        image_{name}_path = join(self.basedir, "{image}")
        if not isfile(image_{name}_path):
            print(image_{name}_path + " " +  _("is missing"))
            exit()
        with Image.open(image_{name}_path) as jpeg:
            image_{name} = ImageTk.PhotoImage(jpeg)
            self.{name}.config(image=image_{name})
            self.{name}.image = image_{name}"""

    class_tpl = """import tkinter as tk
# from PIL import Image, ImageTk
from tkinter import ttk
import gettext
from os.path import join, abspath, dirname, isfile

# fr = gettext.translation('messages', localedir='locale', languages=['fr'])
# _ = fr.gettext
_ = gettext.gettext

class {proj}Base:

    def __init__(self, master):
        self.master = master
        self.basedir = dirname(abspath(__file__))
        style = ttk.Style()
        {styles}
        {variables}
        {attributes}
        {images}
        {grids}

    {methods}

    """

    def __init__(self, fconf):
        self.dest_dir = dirname(abspath(fconf))
        p = fconf.rindex(".")
        s = fconf.rindex("/") + 1
        self.filename = fconf[s:p]
        self.widget_names = ["Notebook", "Frame", "Label", "Entry", "Progressbar",
                             "Combobox", "Treeview", "Checkbutton", "Listbox",
                             "Radiobutton", "Scale", "Spinbox", "LabelFrame",
                             "Text", "Listbox", "Button", "Message"]

        self.defs = {"Treeview": self.widget_treeview,
                     "Frame": self.widget_frame,
                     "LabelFrame": self.widget_labelframe,
                     "Button": self.widget_button,
                     "Radiobutton": self.widget_button,
                     "Checkbutton": self.widget_button
                     }
        self._ttk = ("Button", "Checkbutton", "Entry", "Frame", "Label",
                     "LabelFrame", "Combobox", "Treeview", "Progressbar",
                     "Notebook", "Radiobutton")
        self.attributes, self.images, self.methods,\
            self.grids, self.variables, self.styles = (
            [], [], [], [], [], [])
        yconf = self.get_conf(fconf)
        yconf_style = self.get_style_conf()
        if yconf_style is not None:
            self.set_styles(yconf_style)
        self.run(yconf)
        self.write_file()

    @property
    def proj_name(self):
        if "_" not in self.filename:
            return self.filename.capitalize()
        names = self.filename.split("_")
        return "".join([n.capitalize() for n in names])

    def write_file(self):
        variables = set(self.variables)
        self.variables = list(variables)
        methods = set(self.methods)
        self.methods = list(methods)
        tab = "\n" + " " * 8
        output = self.class_tpl.format(proj=self.proj_name,
                                       attributes="".join(self.attributes),
                                       grids="".join(self.grids),
                                       variables="".join(self.variables),
                                       methods="".join(self.methods),
                                       styles=tab.join(self.styles),
                                       images="".join(self.images))
        fn = join(self.dest_dir, self.filename + "_base.py")
        with open(fn, "w") as fh:
            fh.write(output)

    def get_conf(self, fconf):
        if not isfile(fconf):
            print("Manque le fichier de conf YAML")
            return
        with open(fconf, "r") as fh:
            s = fh.read()
            return yaml.load(s, Loader=yaml.FullLoader)

    def get_style_conf(self):
        fstyle = join(self.dest_dir, "{}_style.yml".format(self.filename))
        if isfile(fstyle):
            with open(fstyle, "r") as fh:
                s = fh.read()
                return yaml.load(s, Loader=yaml.FullLoader)

    def run(self, yconf, parent="master"):
        yconf = OrderedDict(yconf)
        for name, attrs in yconf.items():
            if name not in ("grid", "widget"):
                _type = attrs.get('type', None)
                if _type is None:
                    raise KeyError("Il manque un type à {}".format(name))
                if _type not in self.widget_names:
                    raise KeyError("{} n'est pas un type connu pour {}"
                                   .format(_type, name))
                self.defs.get(_type, self.widget_default)(name, _type,
                                                          attrs, parent)
                fields = attrs.get('fields', None)
                if fields is not None:
                    self.run(fields, name)

    def widget_frame(self, name, _type, attrs, parent):
        self.widget_default(name, _type, attrs, parent)
        grid_columnconfigure = attrs.get("grid_columnconfigure", [])
        if grid_columnconfigure is None:
            raise KeyError("Il manque 'grid_columnconfigure' à la frame {}"
                           .format(name))
        if False in [isinstance(c, list) and len(c)==2 for c in grid_columnconfigure]:
            raise KeyError("'grid_columnconfigure' incomplète à la frame {}"
                           .format(name))
        for i, (w, u) in enumerate(grid_columnconfigure):
            r = self.grid_colconf_tpl.format(i=i, name=name,
                                             weight=w, uniform=u)
            self.grids.append(r)
        grid_rowconfigure = attrs.get("grid_rowconfigure", [])
        if grid_rowconfigure is None:
            raise KeyError("Il manque 'grid_rowconfigure' à la frame {}"
                           .format(name))
        if False in [isinstance(c, list) and len(c)==2 for c in grid_rowconfigure]:
            raise KeyError("'grid_rowconfigure' incomplète à la frame {}"
                           .format(name))
        for i, (w, u) in enumerate(grid_rowconfigure):
            r = self.grid_rowconf_tpl.format(name=name, i=i, weight=w,
                                             uniform=u)
            self.grids.append(r)

    def set_variable(self, name, _type, attrs):
        var = attrs.get("variable", None)
        if var is None:
            return
        if "," not in var:
            raise KeyError("La variable de {} n'est pas TYPE,NOM"
                            .format(name))
        (var_type, var_name) = var.split(",")
        var_types = ("StringVar", "IntVar", "BooleanVar", "DoubleVar")
        if var_type not in var_types:
            raise KeyError("{} n'est pas un de ces choix {}"
                            .format(var_type, var_types))
        self.variables.append(self.variable_tpl.format(name=var_name,
                                                        var_type=var_type))
        return "variable=self.{}".format(var_name)

    def set_attributes(self, name, _type, attrs):
        attrs = attrs.get("widget", None)
        if attrs is None:
            return ""
        w_attrs = []
        for k, v in attrs.items():
            if k not in ("bind", "variable", "image"):
                if k == "command":
                    w = '{}=self.{}'.format(k, v)
                elif isinstance(v, int):
                    w = '{}={}'.format(k, v)
                elif "." in v:
                    w = '{}="{}"'.format(k, v)
                else:
                    w = '{}=_("{}")'.format(k, v)
                w_attrs.append(w)
        self.set_image(name, attrs)
        variable = self.set_variable(name, _type, attrs)
        if variable is not None:
            w_attrs.append(variable)
        if len(w_attrs) > 0:
            return ", " + ",".join(w_attrs)
        return ""

    def set_grid_attributes(self, name, attrs):
        g = attrs.get("grid", None)
        g_attrs = []
        if g is not None:
            for k, v in g.items():
                if k == "sticky":
                    g_attrs.append('{}="{}"'.format(k, v))
                else:
                    g_attrs.append('{}={}'.format(k, v))
            g_attrs = ",".join(g_attrs)
            r = self.grid_tpl.format(name=name, attributes=g_attrs)
            self.grids.append(r)

    def set_styles(self, yconf):
        for name, attrs in yconf.items():
            args = []
            for (k, v)in attrs.items():
                if isinstance(v, str):
                    args.append('{}="{}"'.format(k, v))
                else:
                    args.append('{}={}'.format(k, v))
            st = 'style.configure("{}", {})'.format( name, ",".join(args))
            self.styles.append(st)


    def missing_widget(self, name, attrs):
        try:
            attrs["widget"]
        except KeyError:
            print("Manque la clé 'widget' à {}".format(name))

    def widget_default(self, name, _type, attrs, parent):
        parent = self.get_parent(parent)
        _tk = "ttk" if _type in self._ttk else "tk"
        self.missing_widget(name, attrs)
        w_attrs = self.set_attributes(name, _type, attrs)
        r = self.default_tpl.format(name=name,
                                    tk=_tk,
                                    _type=_type,
                                    parent=parent,
                                    attributes=w_attrs)
        self.attributes.append(r)
        self.set_grid_attributes(name, attrs)
        self.bind_widget(name, attrs)

    def widget_button(self, name, _type, attrs, parent):
        if "command" not in attrs["widget"]:
            attrs["widget"]["command"] = "{}_command".format(name)
            r = self.button_command_tpl.format(name=name)
        else:
            command = attrs["widget"]["command"]
            r = self.command_tpl.format(command=command)
        self.widget_default(name, _type, attrs, parent)
        self.methods.append(r)

    def set_image(self, name, attrs):
        image = attrs.get("image", None)
        if image is not None:
            r = self.image_tpl.format(name=name, image=image)
            self.images.append(r)

    def widget_labelframe(self, name, _type, attrs, parent):
        self.widget_frame(name, _type, attrs, parent)

    def widget_treeview(self, name, _type, attrs, parent):
        parent = self.get_parent(parent)
        self.missing_widget(name, attrs)
        text = attrs['widget'].get("text", '')
        if text != '':
            del(attrs["widget"]["text"])
        w_attrs = self.set_attributes(name, _type, attrs)
        r = self.treeview_tpl.format(name=name,
                                     parent=parent,
                                     attributes=w_attrs,
                                     text=text)
        self.attributes.append(r)
        self.set_grid_attributes(name, attrs)
        self.bind_widget(name, attrs)

    def bind_widget(self, name, attrs):
        bind = attrs.get("bind", None)
        # _type = attrs.get("type")
        co = "<<"
        cf = ">>"
        if bind is not None:
            bind_name = "%s_%s" % (name, bind.lower().replace("-", "_"))
            bd = bind.split(",")
            if len(bd) == 2:
                bind = bd[0]
                bind_name = bd[1]
            if bind in ['Button-1', 'Button-2', 'Button-3',
                        'Leave', 'Key', 'Return']:
                co = "<"
                cf = ">"
            r = self.bind_tpl.format(name=name, bind=bind,
                                     bind_name=bind_name,
                                     co=co, cf=cf)
            self.attributes.append(r)
            r = self.bind_method_tpl.format(bind_name=bind_name)
            self.methods.append(r)

    def get_parent(self, parent):
        if parent != "master":
            parent = "self.{}".format(parent)
        return parent

if len(sys.argv) != 2:
    print("Un fichier yaml de configuraion est attendu")
else:
    Gen(sys.argv[1])
